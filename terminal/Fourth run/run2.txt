Setting TERRIER_HOME to /home/elena/terrier-core-4.4
Setting JAVA_HOME to /usr
18:41:53.074 [main] INFO  o.t.structures.CompressingMetaIndex - Structure meta reading lookup file into memory
18:41:53.101 [main] INFO  o.t.structures.CompressingMetaIndex - Structure meta loading data file into memory
18:41:53.120 [main] INFO  o.t.a.batchquerying.TRECQuerying - time to intialise index : 0.159
18:41:53.141 [main] INFO  o.t.a.batchquerying.TRECQuerying - 351 : falkland petroleum exploration what information is available on petroleum exploration in the south atlantic near the falkland islands
18:41:53.154 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 351: 'falkland petroleum exploration what information is available on petroleum exploration in the south atlantic near the falkland islands'
18:41:53.186 [main] INFO  o.t.matching.PostingListManager - Query 351 with 14 terms has 14 posting lists
18:41:53.438 [main] INFO  o.t.a.batchquerying.TRECQuerying - Writing results to /home/elena/terrier-core-4.4/var/results/TF_IDF_16.res
18:41:53.455 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.314
18:41:53.455 [main] INFO  o.t.a.batchquerying.TRECQuerying - 352 : british chunnel impact what impact has the chunnel had on the british economy and or the life style of the british
18:41:53.456 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 352: 'british chunnel impact what impact has the chunnel had on the british economy and or the life style of the british'
18:41:53.460 [main] INFO  o.t.matching.PostingListManager - Query 352 with 14 terms has 14 posting lists
18:41:53.757 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.302
18:41:53.757 [main] INFO  o.t.a.batchquerying.TRECQuerying - 353 : antarctica exploration identify systematic explorations and scientific investigations of antarctica current or planned
18:41:53.758 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 353: 'antarctica exploration identify systematic explorations and scientific investigations of antarctica current or planned'
18:41:53.760 [main] INFO  o.t.matching.PostingListManager - Query 353 with 12 terms has 12 posting lists
18:41:53.882 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.125
18:41:53.882 [main] INFO  o.t.a.batchquerying.TRECQuerying - 354 : journalist risks identify instances where a journalist has been put at risk e g killed arrested or taken hostage in the performance of his work
18:41:53.883 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 354: 'journalist risks identify instances where a journalist has been put at risk e g killed arrested or taken hostage in the performance of his work'
18:41:53.888 [main] INFO  o.t.matching.PostingListManager - Query 354 with 24 terms has 24 posting lists
18:41:54.264 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.382
18:41:54.264 [main] INFO  o.t.a.batchquerying.TRECQuerying - 355 : ocean remote sensing identify documents discussing the development and application of spaceborne ocean remote sensing
18:41:54.264 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 355: 'ocean remote sensing identify documents discussing the development and application of spaceborne ocean remote sensing'
18:41:54.267 [main] INFO  o.t.matching.PostingListManager - Query 355 with 12 terms has 12 posting lists
18:41:54.431 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.167
18:41:54.431 [main] INFO  o.t.a.batchquerying.TRECQuerying - 356 : postmenopausal estrogen britain identify documents discussing the use of estrogen by postmenopausal women in britain
18:41:54.432 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 356: 'postmenopausal estrogen britain identify documents discussing the use of estrogen by postmenopausal women in britain'
18:41:54.435 [main] INFO  o.t.matching.PostingListManager - Query 356 with 12 terms has 12 posting lists
18:41:54.627 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.196
18:41:54.627 [main] INFO  o.t.a.batchquerying.TRECQuerying - 357 : territorial waters dispute identify documents discussing international boundary disputes relevant to the 200 mile special economic zones or 12 mile territorial waters subsequent to the passing of the international convention on the law of the sea
18:41:54.628 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 357: 'territorial waters dispute identify documents discussing international boundary disputes relevant to the 200 mile special economic zones or 12 mile territorial waters subsequent to the passing of the international convention on the law of the sea'
18:41:54.632 [main] INFO  o.t.matching.PostingListManager - Query 357 with 26 terms has 26 posting lists
18:41:54.890 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.263
18:41:54.890 [main] INFO  o.t.a.batchquerying.TRECQuerying - 358 : blood alcohol fatalities what role does blood alcohol level play in automobile accident fatalities
18:41:54.890 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 358: 'blood alcohol fatalities what role does blood alcohol level play in automobile accident fatalities'
18:41:54.892 [main] INFO  o.t.matching.PostingListManager - Query 358 with 11 terms has 11 posting lists
18:41:54.979 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.089
18:41:54.979 [main] INFO  o.t.a.batchquerying.TRECQuerying - 359 : mutual fund predictors are there reliable and consistent predictors of mutual fund performance
18:41:54.980 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 359: 'mutual fund predictors are there reliable and consistent predictors of mutual fund performance'
18:41:54.982 [main] INFO  o.t.matching.PostingListManager - Query 359 with 10 terms has 10 posting lists
18:41:55.123 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.144
18:41:55.123 [main] INFO  o.t.a.batchquerying.TRECQuerying - 360 : drug legalization benefits what are the benefits if any of drug legalization
18:41:55.123 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 360: 'drug legalization benefits what are the benefits if any of drug legalization'
18:41:55.125 [main] INFO  o.t.matching.PostingListManager - Query 360 with 9 terms has 9 posting lists
18:41:55.279 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.156
18:41:55.279 [main] INFO  o.t.a.batchquerying.TRECQuerying - 361 : clothing sweatshops identify documents that discuss clothing sweatshops
18:41:55.280 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 361: 'clothing sweatshops identify documents that discuss clothing sweatshops'
18:41:55.281 [main] INFO  o.t.matching.PostingListManager - Query 361 with 6 terms has 6 posting lists
18:41:55.318 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.039
18:41:55.318 [main] INFO  o.t.a.batchquerying.TRECQuerying - 362 : human smuggling identify incidents of human smuggling
18:41:55.319 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 362: 'human smuggling identify incidents of human smuggling'
18:41:55.320 [main] INFO  o.t.matching.PostingListManager - Query 362 with 5 terms has 5 posting lists
18:41:55.366 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.048
18:41:55.366 [main] INFO  o.t.a.batchquerying.TRECQuerying - 363 : transportation tunnel disasters what disasters have occurred in tunnels used for transportation
18:41:55.367 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 363: 'transportation tunnel disasters what disasters have occurred in tunnels used for transportation'
18:41:55.369 [main] INFO  o.t.matching.PostingListManager - Query 363 with 10 terms has 10 posting lists
18:41:55.483 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.117
18:41:55.483 [main] INFO  o.t.a.batchquerying.TRECQuerying - 364 : rabies identify documents discussing cases where rabies have been confirmed and what if anything is being done about it
18:41:55.483 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 364: 'rabies identify documents discussing cases where rabies have been confirmed and what if anything is being done about it'
18:41:55.487 [main] INFO  o.t.matching.PostingListManager - Query 364 with 18 terms has 18 posting lists
18:41:55.706 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.223
18:41:55.706 [main] INFO  o.t.a.batchquerying.TRECQuerying - 365 : el nino what effects have been attributed to el nino
18:41:55.707 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 365: 'el nino what effects have been attributed to el nino'
18:41:55.709 [main] INFO  o.t.matching.PostingListManager - Query 365 with 8 terms has 8 posting lists
18:41:55.802 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.096
18:41:55.803 [main] INFO  o.t.a.batchquerying.TRECQuerying - 366 : commercial cyanide uses what are the industrial or commercial uses of cyanide or its derivatives
18:41:55.803 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 366: 'commercial cyanide uses what are the industrial or commercial uses of cyanide or its derivatives'
18:41:55.805 [main] INFO  o.t.matching.PostingListManager - Query 366 with 11 terms has 11 posting lists
18:41:55.970 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.167
18:41:55.970 [main] INFO  o.t.a.batchquerying.TRECQuerying - 367 : piracy what modern instances have there been of old fashioned piracy the boarding or taking control of boats
18:41:55.970 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 367: 'piracy what modern instances have there been of old fashioned piracy the boarding or taking control of boats'
18:41:55.973 [main] INFO  o.t.matching.PostingListManager - Query 367 with 16 terms has 16 posting lists
18:41:56.185 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.215
18:41:56.185 [main] INFO  o.t.a.batchquerying.TRECQuerying - 368 : in vitro fertilization identify documents that discuss in vitro fertilization
18:41:56.186 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 368: 'in vitro fertilization identify documents that discuss in vitro fertilization'
18:41:56.187 [main] INFO  o.t.matching.PostingListManager - Query 368 with 7 terms has 7 posting lists
18:41:56.280 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.095
18:41:56.280 [main] INFO  o.t.a.batchquerying.TRECQuerying - 369 : anorexia nervosa bulimia what are the causes and treatments of anorexia nervosa and bulimia
18:41:56.281 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 369: 'anorexia nervosa bulimia what are the causes and treatments of anorexia nervosa and bulimia'
18:41:56.283 [main] INFO  o.t.matching.PostingListManager - Query 369 with 10 terms has 10 posting lists
18:41:56.461 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.181
18:41:56.461 [main] INFO  o.t.a.batchquerying.TRECQuerying - 370 : food drug laws what are the laws dealing with the quality and processing of food beverages or drugs
18:41:56.462 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 370: 'food drug laws what are the laws dealing with the quality and processing of food beverages or drugs'
18:41:56.465 [main] INFO  o.t.matching.PostingListManager - Query 370 with 15 terms has 15 posting lists
18:41:56.707 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.246
18:41:56.707 [main] INFO  o.t.a.batchquerying.TRECQuerying - 371 : health insurance holistic what is the extent of health insurance coverage of holistic or other non traditional medicine medical treatments for example acupuncture
18:41:56.708 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 371: 'health insurance holistic what is the extent of health insurance coverage of holistic or other non traditional medicine medical treatments for example acupuncture'
18:41:56.711 [main] INFO  o.t.matching.PostingListManager - Query 371 with 19 terms has 19 posting lists
18:41:56.947 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.24
18:41:56.947 [main] INFO  o.t.a.batchquerying.TRECQuerying - 372 : native american casino identify documents that discuss the growth of native american casino gambling
18:41:56.948 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 372: 'native american casino identify documents that discuss the growth of native american casino gambling'
18:41:56.950 [main] INFO  o.t.matching.PostingListManager - Query 372 with 11 terms has 11 posting lists
18:41:57.082 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.135
18:41:57.082 [main] INFO  o.t.a.batchquerying.TRECQuerying - 373 : encryption equipment export identify documents that discuss the concerns of the united states regarding the export of encryption equipment
18:41:57.082 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 373: 'encryption equipment export identify documents that discuss the concerns of the united states regarding the export of encryption equipment'
18:41:57.084 [main] INFO  o.t.matching.PostingListManager - Query 373 with 13 terms has 13 posting lists
18:41:57.227 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.145
18:41:57.227 [main] INFO  o.t.a.batchquerying.TRECQuerying - 374 : nobel prize winners identify and provide background information on nobel prize winners
18:41:57.228 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 374: 'nobel prize winners identify and provide background information on nobel prize winners'
18:41:57.229 [main] INFO  o.t.matching.PostingListManager - Query 374 with 9 terms has 9 posting lists
18:41:57.321 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.094
18:41:57.321 [main] INFO  o.t.a.batchquerying.TRECQuerying - 375 : hydrogen energy what is the status of research on hydrogen as a feasible energy source
18:41:57.322 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 375: 'hydrogen energy what is the status of research on hydrogen as a feasible energy source'
18:41:57.324 [main] INFO  o.t.matching.PostingListManager - Query 375 with 13 terms has 13 posting lists
18:41:57.562 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.241
18:41:57.562 [main] INFO  o.t.a.batchquerying.TRECQuerying - 376 : world court what types of cases were heard by the world court international court of justice
18:41:57.563 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 376: 'world court what types of cases were heard by the world court international court of justice'
18:41:57.565 [main] INFO  o.t.matching.PostingListManager - Query 376 with 12 terms has 12 posting lists
18:41:57.737 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.175
18:41:57.737 [main] INFO  o.t.a.batchquerying.TRECQuerying - 377 : cigar smoking identify documents that discuss the renewed popularity of cigar smoking
18:41:57.737 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 377: 'cigar smoking identify documents that discuss the renewed popularity of cigar smoking'
18:41:57.739 [main] INFO  o.t.matching.PostingListManager - Query 377 with 10 terms has 10 posting lists
18:41:57.877 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.14
18:41:57.877 [main] INFO  o.t.a.batchquerying.TRECQuerying - 378 : euro opposition identify documents that discuss opposition to the introduction of the euro the european currency
18:41:57.878 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 378: 'euro opposition identify documents that discuss opposition to the introduction of the euro the european currency'
18:41:57.880 [main] INFO  o.t.matching.PostingListManager - Query 378 with 12 terms has 12 posting lists
18:41:58.061 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.184
18:41:58.061 [main] INFO  o.t.a.batchquerying.TRECQuerying - 379 : mainstreaming identify documents that discuss mainstreaming children with physical or mental impairments
18:41:58.061 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 379: 'mainstreaming identify documents that discuss mainstreaming children with physical or mental impairments'
18:41:58.063 [main] INFO  o.t.matching.PostingListManager - Query 379 with 11 terms has 11 posting lists
18:41:58.150 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.089
18:41:58.150 [main] INFO  o.t.a.batchquerying.TRECQuerying - 380 : obesity medical treatment identify documents that discuss medical treatment of obesity
18:41:58.150 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 380: 'obesity medical treatment identify documents that discuss medical treatment of obesity'
18:41:58.152 [main] INFO  o.t.matching.PostingListManager - Query 380 with 8 terms has 8 posting lists
18:41:58.231 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.081
18:41:58.231 [main] INFO  o.t.a.batchquerying.TRECQuerying - 381 : alternative medicine what forms of alternative medicine are being used in the treatment of illnesses or diseases and how successful are they
18:41:58.231 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 381: 'alternative medicine what forms of alternative medicine are being used in the treatment of illnesses or diseases and how successful are they'
18:41:58.234 [main] INFO  o.t.matching.PostingListManager - Query 381 with 18 terms has 18 posting lists
18:41:58.509 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.278
18:41:58.509 [main] INFO  o.t.a.batchquerying.TRECQuerying - 382 : hydrogen fuel automobiles identify documents that discuss the use of hydrogen as a fuel for piston driven automobiles safe storage a concern or the use of hydrogen in fuel cells to generate electricity to drive the car
18:41:58.509 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 382: 'hydrogen fuel automobiles identify documents that discuss the use of hydrogen as a fuel for piston driven automobiles safe storage a concern or the use of hydrogen in fuel cells to generate electricity to drive the car'
18:41:58.513 [main] INFO  o.t.matching.PostingListManager - Query 382 with 26 terms has 26 posting lists
18:41:58.904 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.395
18:41:58.904 [main] INFO  o.t.a.batchquerying.TRECQuerying - 383 : mental illness drugs identify drugs used in the treatment of mental illness
18:41:58.905 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 383: 'mental illness drugs identify drugs used in the treatment of mental illness'
18:41:58.908 [main] INFO  o.t.matching.PostingListManager - Query 383 with 9 terms has 9 posting lists
18:41:59.060 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.156
18:41:59.060 [main] INFO  o.t.a.batchquerying.TRECQuerying - 384 : space station moon identify documents that discuss the building of a space station with the intent of colonizing the moon
18:41:59.060 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 384: 'space station moon identify documents that discuss the building of a space station with the intent of colonizing the moon'
18:41:59.063 [main] INFO  o.t.matching.PostingListManager - Query 384 with 14 terms has 14 posting lists
18:41:59.286 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.225
18:41:59.286 [main] INFO  o.t.a.batchquerying.TRECQuerying - 385 : hybrid fuel cars identify documents that discuss the current status of hybrid automobile engines i e cars fueled by something other than gasoline only
18:41:59.286 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 385: 'hybrid fuel cars identify documents that discuss the current status of hybrid automobile engines i e cars fueled by something other than gasoline only'
18:41:59.290 [main] INFO  o.t.matching.PostingListManager - Query 385 with 22 terms has 22 posting lists
18:41:59.524 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.238
18:41:59.525 [main] INFO  o.t.a.batchquerying.TRECQuerying - 386 : teaching disabled children what methods are currently utilized or anticipated in the teaching of disabled children
18:41:59.525 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 386: 'teaching disabled children what methods are currently utilized or anticipated in the teaching of disabled children'
18:41:59.527 [main] INFO  o.t.matching.PostingListManager - Query 386 with 13 terms has 13 posting lists
18:41:59.717 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.192
18:41:59.717 [main] INFO  o.t.a.batchquerying.TRECQuerying - 387 : radioactive waste identify documents that discuss effective and safe ways to permanently handle long lived radioactive wastes
18:41:59.717 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 387: 'radioactive waste identify documents that discuss effective and safe ways to permanently handle long lived radioactive wastes'
18:41:59.720 [main] INFO  o.t.matching.PostingListManager - Query 387 with 16 terms has 16 posting lists
18:41:59.852 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.135
18:41:59.853 [main] INFO  o.t.a.batchquerying.TRECQuerying - 388 : organic soil enhancement identify documents that discuss the use of organic fertilizers composted sludge ash vegetable waste microorganisms etc as soil enhancers
18:41:59.853 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 388: 'organic soil enhancement identify documents that discuss the use of organic fertilizers composted sludge ash vegetable waste microorganisms etc as soil enhancers'
18:41:59.856 [main] INFO  o.t.matching.PostingListManager - Query 388 with 20 terms has 20 posting lists
18:42:00.015 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.162
18:42:00.015 [main] INFO  o.t.a.batchquerying.TRECQuerying - 389 : illegal technology transfer what specific entities have been accused of illegal technology transfer such as selling their products formulas etc directly or indirectly to foreign entities for other than peaceful purposes
18:42:00.016 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 389: 'illegal technology transfer what specific entities have been accused of illegal technology transfer such as selling their products formulas etc directly or indirectly to foreign entities for other than peaceful purposes'
18:42:00.020 [main] INFO  o.t.matching.PostingListManager - Query 389 with 27 terms has 27 posting lists
18:42:00.347 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.332
18:42:00.347 [main] INFO  o.t.a.batchquerying.TRECQuerying - 390 : orphan drugs find documents that discuss issues associated with so called orphan drugs that is drugs that treat diseases affecting relatively few people
18:42:00.347 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 390: 'orphan drugs find documents that discuss issues associated with so called orphan drugs that is drugs that treat diseases affecting relatively few people'
18:42:00.350 [main] INFO  o.t.matching.PostingListManager - Query 390 with 18 terms has 18 posting lists
18:42:00.492 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.145
18:42:00.493 [main] INFO  o.t.a.batchquerying.TRECQuerying - 391 : r drug prices identify documents that discuss the impact of the cost of research and development r on the price of drugs
18:42:00.493 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 391: 'r drug prices identify documents that discuss the impact of the cost of research and development r on the price of drugs'
18:42:00.495 [main] INFO  o.t.matching.PostingListManager - Query 391 with 17 terms has 17 posting lists
18:42:00.722 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.229
18:42:00.722 [main] INFO  o.t.a.batchquerying.TRECQuerying - 392 : robotics what are the applications of robotics in the world today
18:42:00.722 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 392: 'robotics what are the applications of robotics in the world today'
18:42:00.724 [main] INFO  o.t.matching.PostingListManager - Query 392 with 9 terms has 9 posting lists
18:42:00.896 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.174
18:42:00.896 [main] INFO  o.t.a.batchquerying.TRECQuerying - 393 : mercy killing identify documents that discuss mercy killings
18:42:00.896 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 393: 'mercy killing identify documents that discuss mercy killings'
18:42:00.897 [main] INFO  o.t.matching.PostingListManager - Query 393 with 7 terms has 7 posting lists
18:42:00.934 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.038
18:42:00.934 [main] INFO  o.t.a.batchquerying.TRECQuerying - 394 : home schooling identify documents that discuss the education of children at home home schooling
18:42:00.935 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 394: 'home schooling identify documents that discuss the education of children at home home schooling'
18:42:00.936 [main] INFO  o.t.matching.PostingListManager - Query 394 with 11 terms has 11 posting lists
18:42:01.091 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.157
18:42:01.091 [main] INFO  o.t.a.batchquerying.TRECQuerying - 395 : tourism provide examples of successful attempts to attract tourism as a means to improve a local economy
18:42:01.092 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 395: 'tourism provide examples of successful attempts to attract tourism as a means to improve a local economy'
18:42:01.094 [main] INFO  o.t.matching.PostingListManager - Query 395 with 14 terms has 14 posting lists
18:42:01.271 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.18
18:42:01.271 [main] INFO  o.t.a.batchquerying.TRECQuerying - 396 : sick building syndrome identify documents that discuss sick building syndrome or building related illnesses
18:42:01.271 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 396: 'sick building syndrome identify documents that discuss sick building syndrome or building related illnesses'
18:42:01.273 [main] INFO  o.t.matching.PostingListManager - Query 396 with 10 terms has 10 posting lists
18:42:01.335 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.064
18:42:01.335 [main] INFO  o.t.a.batchquerying.TRECQuerying - 397 : automobile recalls identify documents that discuss the reasons for automobile recalls
18:42:01.335 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 397: 'automobile recalls identify documents that discuss the reasons for automobile recalls'
18:42:01.337 [main] INFO  o.t.matching.PostingListManager - Query 397 with 9 terms has 9 posting lists
18:42:01.452 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.117
18:42:01.452 [main] INFO  o.t.a.batchquerying.TRECQuerying - 398 : dismantling europe s arsenal identify documents that discuss the european conventional arms cut as it relates to the dismantling of europe s arsenal
18:42:01.453 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 398: 'dismantling europe s arsenal identify documents that discuss the european conventional arms cut as it relates to the dismantling of europe s arsenal'
18:42:01.456 [main] INFO  o.t.matching.PostingListManager - Query 398 with 18 terms has 18 posting lists
18:42:01.724 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.272
18:42:01.724 [main] INFO  o.t.a.batchquerying.TRECQuerying - 399 : oceanographic vessels identify documents that discuss the activities or equipment of oceanographic vessels
18:42:01.725 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 399: 'oceanographic vessels identify documents that discuss the activities or equipment of oceanographic vessels'
18:42:01.726 [main] INFO  o.t.matching.PostingListManager - Query 399 with 11 terms has 11 posting lists
18:42:01.870 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.146
18:42:01.870 [main] INFO  o.t.a.batchquerying.TRECQuerying - 400 : amazon rain forest what measures are being taken by local south american authorities to preserve the amazon tropical rain forest
18:42:01.871 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 400: 'amazon rain forest what measures are being taken by local south american authorities to preserve the amazon tropical rain forest'
18:42:01.873 [main] INFO  o.t.matching.PostingListManager - Query 400 with 17 terms has 17 posting lists
18:42:02.064 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.194
18:42:02.070 [main] INFO  o.t.a.batchquerying.TRECQuerying - Settings of Terrier written to /home/elena/terrier-core-4.4/var/results/TF_IDF_16.res.settings
18:42:02.070 [main] INFO  o.t.a.batchquerying.TRECQuerying - Finished topics, executed 50 queries in 8.93 seconds, results written to /home/elena/terrier-core-4.4/var/results/TF_IDF_16.res
Time elapsed: 9.112 seconds.
