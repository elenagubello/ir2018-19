import os
import os.path
import patoolib
from shutil import copyfile, move

output = os.path.join(os.getcwd(), 'collections')
print(output)
x = input()
dtds = os.path.join(output, 'DTDS')
print(dtds)
x = input()

# clear output directory
for dirpath, dirnames, filenames in os.walk(output):
    for filename in filenames:
        os.remove(os.path.join(dirpath, filename))
print('svuotato')
for dirpath, dirnames, filenames in os.walk(os.path.join(os.getcwd(), 'TIPSTER')):
    for filename in filenames:
        end = filename.split('.')[-1]
        if end[-1] == 'Z':
            if len(end) == 2: # files with wrong extension (e.g. ".2Z")
                newname = filename[:-3] + filename[-2] + '.' + filename[-1:]
                os.rename(os.path.join(dirpath, filename), os.path.join(dirpath, newname))
                filename = newname
            ar = dirpath + '/' + filename
            patoolib.extract_archive(ar, outdir=output)
            filename = filename[:-2]
            
        # DTD files
            if filename[-3:] == 'DTD': # DTD files without separator for extension
                newname = filename[:-3] + '.DTD'
                os.rename(os.path.join(output, filename), os.path.join(output, newname))
                filename = newname
                move(os.path.join(output, filename), os.path.join(dtds, filename))
        elif end == 'DTD': # copying DTD files in dtds directory
            copyfile(os.path.join(dirpath, filename), os.path.join(dtds, filename))
            
