Setting TERRIER_HOME to /home/elena/terrier-core-4.4
Setting JAVA_HOME to /usr
18:18:08.880 [main] INFO  o.t.structures.CompressingMetaIndex - Structure meta reading lookup file into memory
18:18:08.904 [main] INFO  o.t.structures.CompressingMetaIndex - Structure meta loading data file into memory
18:18:08.923 [main] INFO  o.t.a.batchquerying.TRECQuerying - time to intialise index : 0.156
18:18:08.948 [main] INFO  o.t.a.batchquerying.TRECQuerying - 351 : falkland petroleum exploration what information is available on petroleum exploration in the south atlantic near the falkland islands
18:18:08.960 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 351: 'falkland petroleum exploration what information is available on petroleum exploration in the south atlantic near the falkland islands'
18:18:08.991 [main] INFO  o.t.matching.PostingListManager - Query 351 with 7 terms has 7 posting lists
18:18:09.042 [main] INFO  o.t.a.batchquerying.TRECQuerying - Writing results to /home/elena/terrier-core-4.4/var/results/TF_IDF_14.res
18:18:09.058 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.11
18:18:09.059 [main] INFO  o.t.a.batchquerying.TRECQuerying - 352 : british chunnel impact what impact has the chunnel had on the british economy and or the life style of the british
18:18:09.059 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 352: 'british chunnel impact what impact has the chunnel had on the british economy and or the life style of the british'
18:18:09.061 [main] INFO  o.t.matching.PostingListManager - Query 352 with 6 terms has 6 posting lists
18:18:09.094 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.035
18:18:09.094 [main] INFO  o.t.a.batchquerying.TRECQuerying - 353 : antarctica exploration identify systematic explorations and scientific investigations of antarctica current or planned
18:18:09.094 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 353: 'antarctica exploration identify systematic explorations and scientific investigations of antarctica current or planned'
18:18:09.097 [main] INFO  o.t.matching.PostingListManager - Query 353 with 8 terms has 8 posting lists
18:18:09.129 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.035
18:18:09.129 [main] INFO  o.t.a.batchquerying.TRECQuerying - 354 : journalist risks identify instances where a journalist has been put at risk e g killed arrested or taken hostage in the performance of his work
18:18:09.130 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 354: 'journalist risks identify instances where a journalist has been put at risk e g killed arrested or taken hostage in the performance of his work'
18:18:09.136 [main] INFO  o.t.matching.PostingListManager - Query 354 with 11 terms has 11 posting lists
18:18:09.197 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.068
18:18:09.197 [main] INFO  o.t.a.batchquerying.TRECQuerying - 355 : ocean remote sensing identify documents discussing the development and application of spaceborne ocean remote sensing
18:18:09.198 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 355: 'ocean remote sensing identify documents discussing the development and application of spaceborne ocean remote sensing'
18:18:09.200 [main] INFO  o.t.matching.PostingListManager - Query 355 with 9 terms has 9 posting lists
18:18:09.234 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.037
18:18:09.234 [main] INFO  o.t.a.batchquerying.TRECQuerying - 356 : postmenopausal estrogen britain identify documents discussing the use of estrogen by postmenopausal women in britain
18:18:09.234 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 356: 'postmenopausal estrogen britain identify documents discussing the use of estrogen by postmenopausal women in britain'
18:18:09.236 [main] INFO  o.t.matching.PostingListManager - Query 356 with 7 terms has 7 posting lists
18:18:09.257 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.023
18:18:09.257 [main] INFO  o.t.a.batchquerying.TRECQuerying - 357 : territorial waters dispute identify documents discussing international boundary disputes relevant to the 200 mile special economic zones or 12 mile territorial waters subsequent to the passing of the international convention on the law of the sea
18:18:09.258 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 357: 'territorial waters dispute identify documents discussing international boundary disputes relevant to the 200 mile special economic zones or 12 mile territorial waters subsequent to the passing of the international convention on the law of the sea'
18:18:09.262 [main] INFO  o.t.matching.PostingListManager - Query 357 with 20 terms has 20 posting lists
18:18:09.403 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.146
18:18:09.403 [main] INFO  o.t.a.batchquerying.TRECQuerying - 358 : blood alcohol fatalities what role does blood alcohol level play in automobile accident fatalities
18:18:09.403 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 358: 'blood alcohol fatalities what role does blood alcohol level play in automobile accident fatalities'
18:18:09.405 [main] INFO  o.t.matching.PostingListManager - Query 358 with 8 terms has 8 posting lists
18:18:09.427 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.024
18:18:09.427 [main] INFO  o.t.a.batchquerying.TRECQuerying - 359 : mutual fund predictors are there reliable and consistent predictors of mutual fund performance
18:18:09.427 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 359: 'mutual fund predictors are there reliable and consistent predictors of mutual fund performance'
18:18:09.429 [main] INFO  o.t.matching.PostingListManager - Query 359 with 6 terms has 6 posting lists
18:18:09.447 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.02
18:18:09.447 [main] INFO  o.t.a.batchquerying.TRECQuerying - 360 : drug legalization benefits what are the benefits if any of drug legalization
18:18:09.448 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 360: 'drug legalization benefits what are the benefits if any of drug legalization'
18:18:09.449 [main] INFO  o.t.matching.PostingListManager - Query 360 with 3 terms has 3 posting lists
18:18:09.463 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.016
18:18:09.463 [main] INFO  o.t.a.batchquerying.TRECQuerying - 361 : clothing sweatshops identify documents that discuss clothing sweatshops
18:18:09.463 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 361: 'clothing sweatshops identify documents that discuss clothing sweatshops'
18:18:09.465 [main] INFO  o.t.matching.PostingListManager - Query 361 with 5 terms has 5 posting lists
18:18:09.481 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.018
18:18:09.481 [main] INFO  o.t.a.batchquerying.TRECQuerying - 362 : human smuggling identify incidents of human smuggling
18:18:09.482 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 362: 'human smuggling identify incidents of human smuggling'
18:18:09.483 [main] INFO  o.t.matching.PostingListManager - Query 362 with 4 terms has 4 posting lists
18:18:09.493 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.012
18:18:09.493 [main] INFO  o.t.a.batchquerying.TRECQuerying - 363 : transportation tunnel disasters what disasters have occurred in tunnels used for transportation
18:18:09.493 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 363: 'transportation tunnel disasters what disasters have occurred in tunnels used for transportation'
18:18:09.494 [main] INFO  o.t.matching.PostingListManager - Query 363 with 4 terms has 4 posting lists
18:18:09.504 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.011
18:18:09.504 [main] INFO  o.t.a.batchquerying.TRECQuerying - 364 : rabies identify documents discussing cases where rabies have been confirmed and what if anything is being done about it
18:18:09.504 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 364: 'rabies identify documents discussing cases where rabies have been confirmed and what if anything is being done about it'
18:18:09.506 [main] INFO  o.t.matching.PostingListManager - Query 364 with 7 terms has 7 posting lists
18:18:09.542 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.038
18:18:09.542 [main] INFO  o.t.a.batchquerying.TRECQuerying - 365 : el nino what effects have been attributed to el nino
18:18:09.542 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 365: 'el nino what effects have been attributed to el nino'
18:18:09.543 [main] INFO  o.t.matching.PostingListManager - Query 365 with 4 terms has 4 posting lists
18:18:09.554 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.012
18:18:09.554 [main] INFO  o.t.a.batchquerying.TRECQuerying - 366 : commercial cyanide uses what are the industrial or commercial uses of cyanide or its derivatives
18:18:09.554 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 366: 'commercial cyanide uses what are the industrial or commercial uses of cyanide or its derivatives'
18:18:09.555 [main] INFO  o.t.matching.PostingListManager - Query 366 with 5 terms has 5 posting lists
18:18:09.573 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.019
18:18:09.573 [main] INFO  o.t.a.batchquerying.TRECQuerying - 367 : piracy what modern instances have there been of old fashioned piracy the boarding or taking control of boats
18:18:09.574 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 367: 'piracy what modern instances have there been of old fashioned piracy the boarding or taking control of boats'
18:18:09.576 [main] INFO  o.t.matching.PostingListManager - Query 367 with 9 terms has 9 posting lists
18:18:09.609 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.036
18:18:09.609 [main] INFO  o.t.a.batchquerying.TRECQuerying - 368 : in vitro fertilization identify documents that discuss in vitro fertilization
18:18:09.609 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 368: 'in vitro fertilization identify documents that discuss in vitro fertilization'
18:18:09.611 [main] INFO  o.t.matching.PostingListManager - Query 368 with 5 terms has 5 posting lists
18:18:09.623 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.014
18:18:09.623 [main] INFO  o.t.a.batchquerying.TRECQuerying - 369 : anorexia nervosa bulimia what are the causes and treatments of anorexia nervosa and bulimia
18:18:09.623 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 369: 'anorexia nervosa bulimia what are the causes and treatments of anorexia nervosa and bulimia'
18:18:09.625 [main] INFO  o.t.matching.PostingListManager - Query 369 with 5 terms has 5 posting lists
18:18:09.633 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.01
18:18:09.633 [main] INFO  o.t.a.batchquerying.TRECQuerying - 370 : food drug laws what are the laws dealing with the quality and processing of food beverages or drugs
18:18:09.633 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 370: 'food drug laws what are the laws dealing with the quality and processing of food beverages or drugs'
18:18:09.635 [main] INFO  o.t.matching.PostingListManager - Query 370 with 7 terms has 7 posting lists
18:18:09.658 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.025
18:18:09.659 [main] INFO  o.t.a.batchquerying.TRECQuerying - 371 : health insurance holistic what is the extent of health insurance coverage of holistic or other non traditional medicine medical treatments for example acupuncture
18:18:09.659 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 371: 'health insurance holistic what is the extent of health insurance coverage of holistic or other non traditional medicine medical treatments for example acupuncture'
18:18:09.661 [main] INFO  o.t.matching.PostingListManager - Query 371 with 12 terms has 12 posting lists
18:18:09.684 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.025
18:18:09.685 [main] INFO  o.t.a.batchquerying.TRECQuerying - 372 : native american casino identify documents that discuss the growth of native american casino gambling
18:18:09.685 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 372: 'native american casino identify documents that discuss the growth of native american casino gambling'
18:18:09.687 [main] INFO  o.t.matching.PostingListManager - Query 372 with 8 terms has 8 posting lists
18:18:09.708 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.023
18:18:09.708 [main] INFO  o.t.a.batchquerying.TRECQuerying - 373 : encryption equipment export identify documents that discuss the concerns of the united states regarding the export of encryption equipment
18:18:09.709 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 373: 'encryption equipment export identify documents that discuss the concerns of the united states regarding the export of encryption equipment'
18:18:09.711 [main] INFO  o.t.matching.PostingListManager - Query 373 with 9 terms has 9 posting lists
18:18:09.763 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.055
18:18:09.763 [main] INFO  o.t.a.batchquerying.TRECQuerying - 374 : nobel prize winners identify and provide background information on nobel prize winners
18:18:09.764 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 374: 'nobel prize winners identify and provide background information on nobel prize winners'
18:18:09.765 [main] INFO  o.t.matching.PostingListManager - Query 374 with 5 terms has 5 posting lists
18:18:09.775 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.012
18:18:09.775 [main] INFO  o.t.a.batchquerying.TRECQuerying - 375 : hydrogen energy what is the status of research on hydrogen as a feasible energy source
18:18:09.775 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 375: 'hydrogen energy what is the status of research on hydrogen as a feasible energy source'
18:18:09.776 [main] INFO  o.t.matching.PostingListManager - Query 375 with 6 terms has 6 posting lists
18:18:09.795 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.02
18:18:09.795 [main] INFO  o.t.a.batchquerying.TRECQuerying - 376 : world court what types of cases were heard by the world court international court of justice
18:18:09.795 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 376: 'world court what types of cases were heard by the world court international court of justice'
18:18:09.796 [main] INFO  o.t.matching.PostingListManager - Query 376 with 7 terms has 7 posting lists
18:18:09.845 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.05
18:18:09.845 [main] INFO  o.t.a.batchquerying.TRECQuerying - 377 : cigar smoking identify documents that discuss the renewed popularity of cigar smoking
18:18:09.845 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 377: 'cigar smoking identify documents that discuss the renewed popularity of cigar smoking'
18:18:09.847 [main] INFO  o.t.matching.PostingListManager - Query 377 with 7 terms has 7 posting lists
18:18:09.868 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.023
18:18:09.868 [main] INFO  o.t.a.batchquerying.TRECQuerying - 378 : euro opposition identify documents that discuss opposition to the introduction of the euro the european currency
18:18:09.868 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 378: 'euro opposition identify documents that discuss opposition to the introduction of the euro the european currency'
18:18:09.872 [main] INFO  o.t.matching.PostingListManager - Query 378 with 8 terms has 8 posting lists
18:18:09.898 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.03
18:18:09.898 [main] INFO  o.t.a.batchquerying.TRECQuerying - 379 : mainstreaming identify documents that discuss mainstreaming children with physical or mental impairments
18:18:09.898 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 379: 'mainstreaming identify documents that discuss mainstreaming children with physical or mental impairments'
18:18:09.900 [main] INFO  o.t.matching.PostingListManager - Query 379 with 8 terms has 8 posting lists
18:18:09.921 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.023
18:18:09.921 [main] INFO  o.t.a.batchquerying.TRECQuerying - 380 : obesity medical treatment identify documents that discuss medical treatment of obesity
18:18:09.921 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 380: 'obesity medical treatment identify documents that discuss medical treatment of obesity'
18:18:09.922 [main] INFO  o.t.matching.PostingListManager - Query 380 with 6 terms has 6 posting lists
18:18:09.939 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.018
18:18:09.940 [main] INFO  o.t.a.batchquerying.TRECQuerying - 381 : alternative medicine what forms of alternative medicine are being used in the treatment of illnesses or diseases and how successful are they
18:18:09.940 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 381: 'alternative medicine what forms of alternative medicine are being used in the treatment of illnesses or diseases and how successful are they'
18:18:09.942 [main] INFO  o.t.matching.PostingListManager - Query 381 with 8 terms has 8 posting lists
18:18:09.969 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.029
18:18:09.970 [main] INFO  o.t.a.batchquerying.TRECQuerying - 382 : hydrogen fuel automobiles identify documents that discuss the use of hydrogen as a fuel for piston driven automobiles safe storage a concern or the use of hydrogen in fuel cells to generate electricity to drive the car
18:18:09.970 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 382: 'hydrogen fuel automobiles identify documents that discuss the use of hydrogen as a fuel for piston driven automobiles safe storage a concern or the use of hydrogen in fuel cells to generate electricity to drive the car'
18:18:09.973 [main] INFO  o.t.matching.PostingListManager - Query 382 with 16 terms has 16 posting lists
18:18:10.023 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.053
18:18:10.023 [main] INFO  o.t.a.batchquerying.TRECQuerying - 383 : mental illness drugs identify drugs used in the treatment of mental illness
18:18:10.023 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 383: 'mental illness drugs identify drugs used in the treatment of mental illness'
18:18:10.025 [main] INFO  o.t.matching.PostingListManager - Query 383 with 5 terms has 5 posting lists
18:18:10.037 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.014
18:18:10.037 [main] INFO  o.t.a.batchquerying.TRECQuerying - 384 : space station moon identify documents that discuss the building of a space station with the intent of colonizing the moon
18:18:10.037 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 384: 'space station moon identify documents that discuss the building of a space station with the intent of colonizing the moon'
18:18:10.039 [main] INFO  o.t.matching.PostingListManager - Query 384 with 9 terms has 9 posting lists
18:18:10.067 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.03
18:18:10.067 [main] INFO  o.t.a.batchquerying.TRECQuerying - 385 : hybrid fuel cars identify documents that discuss the current status of hybrid automobile engines i e cars fueled by something other than gasoline only
18:18:10.067 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 385: 'hybrid fuel cars identify documents that discuss the current status of hybrid automobile engines i e cars fueled by something other than gasoline only'
18:18:10.070 [main] INFO  o.t.matching.PostingListManager - Query 385 with 12 terms has 12 posting lists
18:18:10.120 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.053
18:18:10.120 [main] INFO  o.t.a.batchquerying.TRECQuerying - 386 : teaching disabled children what methods are currently utilized or anticipated in the teaching of disabled children
18:18:10.120 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 386: 'teaching disabled children what methods are currently utilized or anticipated in the teaching of disabled children'
18:18:10.122 [main] INFO  o.t.matching.PostingListManager - Query 386 with 7 terms has 7 posting lists
18:18:10.141 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.021
18:18:10.141 [main] INFO  o.t.a.batchquerying.TRECQuerying - 387 : radioactive waste identify documents that discuss effective and safe ways to permanently handle long lived radioactive wastes
18:18:10.141 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 387: 'radioactive waste identify documents that discuss effective and safe ways to permanently handle long lived radioactive wastes'
18:18:10.143 [main] INFO  o.t.matching.PostingListManager - Query 387 with 12 terms has 12 posting lists
18:18:10.193 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.052
18:18:10.193 [main] INFO  o.t.a.batchquerying.TRECQuerying - 388 : organic soil enhancement identify documents that discuss the use of organic fertilizers composted sludge ash vegetable waste microorganisms etc as soil enhancers
18:18:10.193 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 388: 'organic soil enhancement identify documents that discuss the use of organic fertilizers composted sludge ash vegetable waste microorganisms etc as soil enhancers'
18:18:10.196 [main] INFO  o.t.matching.PostingListManager - Query 388 with 13 terms has 13 posting lists
18:18:10.218 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.025
18:18:10.218 [main] INFO  o.t.a.batchquerying.TRECQuerying - 389 : illegal technology transfer what specific entities have been accused of illegal technology transfer such as selling their products formulas etc directly or indirectly to foreign entities for other than peaceful purposes
18:18:10.218 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 389: 'illegal technology transfer what specific entities have been accused of illegal technology transfer such as selling their products formulas etc directly or indirectly to foreign entities for other than peaceful purposes'
18:18:10.221 [main] INFO  o.t.matching.PostingListManager - Query 389 with 14 terms has 14 posting lists
18:18:10.267 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.049
18:18:10.267 [main] INFO  o.t.a.batchquerying.TRECQuerying - 390 : orphan drugs find documents that discuss issues associated with so called orphan drugs that is drugs that treat diseases affecting relatively few people
18:18:10.267 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 390: 'orphan drugs find documents that discuss issues associated with so called orphan drugs that is drugs that treat diseases affecting relatively few people'
18:18:10.270 [main] INFO  o.t.matching.PostingListManager - Query 390 with 12 terms has 12 posting lists
18:18:10.331 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.064
18:18:10.331 [main] INFO  o.t.a.batchquerying.TRECQuerying - 391 : r drug prices identify documents that discuss the impact of the cost of research and development r on the price of drugs
18:18:10.331 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 391: 'r drug prices identify documents that discuss the impact of the cost of research and development r on the price of drugs'
18:18:10.332 [main] INFO  o.t.matching.PostingListManager - Query 391 with 9 terms has 9 posting lists
18:18:10.378 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.047
18:18:10.378 [main] INFO  o.t.a.batchquerying.TRECQuerying - 392 : robotics what are the applications of robotics in the world today
18:18:10.378 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 392: 'robotics what are the applications of robotics in the world today'
18:18:10.379 [main] INFO  o.t.matching.PostingListManager - Query 392 with 4 terms has 4 posting lists
18:18:10.402 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.024
18:18:10.402 [main] INFO  o.t.a.batchquerying.TRECQuerying - 393 : mercy killing identify documents that discuss mercy killings
18:18:10.403 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 393: 'mercy killing identify documents that discuss mercy killings'
18:18:10.404 [main] INFO  o.t.matching.PostingListManager - Query 393 with 5 terms has 5 posting lists
18:18:10.418 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.016
18:18:10.418 [main] INFO  o.t.a.batchquerying.TRECQuerying - 394 : home schooling identify documents that discuss the education of children at home home schooling
18:18:10.418 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 394: 'home schooling identify documents that discuss the education of children at home home schooling'
18:18:10.420 [main] INFO  o.t.matching.PostingListManager - Query 394 with 7 terms has 7 posting lists
18:18:10.445 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.027
18:18:10.445 [main] INFO  o.t.a.batchquerying.TRECQuerying - 395 : tourism provide examples of successful attempts to attract tourism as a means to improve a local economy
18:18:10.445 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 395: 'tourism provide examples of successful attempts to attract tourism as a means to improve a local economy'
18:18:10.447 [main] INFO  o.t.matching.PostingListManager - Query 395 with 9 terms has 9 posting lists
18:18:10.483 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.038
18:18:10.484 [main] INFO  o.t.a.batchquerying.TRECQuerying - 396 : sick building syndrome identify documents that discuss sick building syndrome or building related illnesses
18:18:10.484 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 396: 'sick building syndrome identify documents that discuss sick building syndrome or building related illnesses'
18:18:10.485 [main] INFO  o.t.matching.PostingListManager - Query 396 with 7 terms has 7 posting lists
18:18:10.504 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.02
18:18:10.505 [main] INFO  o.t.a.batchquerying.TRECQuerying - 397 : automobile recalls identify documents that discuss the reasons for automobile recalls
18:18:10.505 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 397: 'automobile recalls identify documents that discuss the reasons for automobile recalls'
18:18:10.506 [main] INFO  o.t.matching.PostingListManager - Query 397 with 6 terms has 6 posting lists
18:18:10.525 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.02
18:18:10.525 [main] INFO  o.t.a.batchquerying.TRECQuerying - 398 : dismantling europe s arsenal identify documents that discuss the european conventional arms cut as it relates to the dismantling of europe s arsenal
18:18:10.525 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 398: 'dismantling europe s arsenal identify documents that discuss the european conventional arms cut as it relates to the dismantling of europe s arsenal'
18:18:10.527 [main] INFO  o.t.matching.PostingListManager - Query 398 with 11 terms has 11 posting lists
18:18:10.566 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.041
18:18:10.566 [main] INFO  o.t.a.batchquerying.TRECQuerying - 399 : oceanographic vessels identify documents that discuss the activities or equipment of oceanographic vessels
18:18:10.567 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 399: 'oceanographic vessels identify documents that discuss the activities or equipment of oceanographic vessels'
18:18:10.568 [main] INFO  o.t.matching.PostingListManager - Query 399 with 7 terms has 7 posting lists
18:18:10.594 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.028
18:18:10.595 [main] INFO  o.t.a.batchquerying.TRECQuerying - 400 : amazon rain forest what measures are being taken by local south american authorities to preserve the amazon tropical rain forest
18:18:10.595 [main] INFO  o.t.a.batchquerying.TRECQuerying - Processing query: 400: 'amazon rain forest what measures are being taken by local south american authorities to preserve the amazon tropical rain forest'
18:18:10.597 [main] INFO  o.t.matching.PostingListManager - Query 400 with 12 terms has 12 posting lists
18:18:10.650 [main] INFO  o.t.a.batchquerying.TRECQuerying - Time to process query: 0.055
18:18:10.656 [main] INFO  o.t.a.batchquerying.TRECQuerying - Settings of Terrier written to /home/elena/terrier-core-4.4/var/results/TF_IDF_14.res.settings
18:18:10.656 [main] INFO  o.t.a.batchquerying.TRECQuerying - Finished topics, executed 50 queries in 1.709 seconds, results written to /home/elena/terrier-core-4.4/var/results/TF_IDF_14.res
Time elapsed: 1.892 seconds.
