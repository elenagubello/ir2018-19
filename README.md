##### Elena Gubello 1152566 #####
#### Reperimento dell'Informazione 2018-19 ####
# Homework 1 #

Partendo dalla cartella compressa TIPSTER, i file della collezione vengono estratti e organizzati per cartelle usando "Extraction.py".

La collezione TREC7 viene quindi indicizzata e analizzata usando Terrier e trec_eval.

Nel file "trec.txt" sono presenti i comandi utilizzati da terminale per costruire i vari indici e per eseguire le run.

Nella cartella "terminal" sono presenti gli output generati da quei comandi, suddivisi per run, compresi i file "terrier.properties" specifici.

In particolare, i numeri alla fine del nome di ciascun file indicano il setup relativo:

1.	TrecQueryTags.process=TITLE - TrecQueryTags.skip=DESC,NARR
2.	TrecQueryTags.process=TITLE,DESC - TrecQueryTags.skip=NARR
3.	TrecQueryTags.process=TITLE,DESC,NARR - TrecQueryTags.skip=

Nel file "results.txt" sono riassunti i risultati delle valutazioni, riportando i valori della categoria 'all' per ciascuna run.

I notebook presenti sono quelli usati per condurre i test statistici e per creare i grafici presenti nella cartella "images".

In "Map_ANOVA.ipynb" viene eseguito il test Anova 1-way evidenziando i risultati intermedi del procedimento, senza utilizzare librerie specifiche per condurre il test.

In "k_f_oneway.ipynb" (dove k indica il numero relativo al setup analizzato) viene condotto lo stesso test Anova 1-way ma usando la funzione f_oneway del pacchetto scipy.

In "k_Tukey.ipynb" viene condotto il test Tukey HSD utilizzando la funzione MultiComparison del pacchetto statsmodels.

In "k_Plot.ipynb" viene salvato il grafico con il confronto tra i boxplot relativi al MAP di ciascuna run.